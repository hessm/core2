package core

import (
	"fmt"
	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/types"
	"go.uber.org/zap"
	zc "go.uber.org/zap/zapcore"
	"io"
)

// NewLogger returns a new Logger instance with specified logLevel and devMode.
//
// The writer can be used e.g. to save the logs in a file.
func NewLogger(logLevel string, isDev bool, writer ...io.Writer) (*types.Logger, error) {
	level, err := parseLogLevel(logLevel)
	if err != nil {
		return nil, fmt.Errorf("error parsing logLevel: %w", err)
	}

	logger, err := getLoggerImplementation(writer, level, isDev)
	return &types.Logger{Logger: logger}, nil
}

func getLoggerImplementation(writer []io.Writer, level *zap.AtomicLevel, isDev bool) (logr.Logger, error) {
	sample := zap.NewDevelopmentConfig()
	config := zap.Config{
		Level:             *level,
		Development:       isDev,
		DisableCaller:     false,
		DisableStacktrace: false,

		// rest of config fields are taken from `zap.NewDevelopmentConfig()`
		Sampling:         sample.Sampling,
		Encoding:         sample.Encoding,
		EncoderConfig:    sample.EncoderConfig,
		OutputPaths:      sample.OutputPaths,
		ErrorOutputPaths: sample.ErrorOutputPaths,
		InitialFields:    sample.InitialFields,
	}

	opts := getLoggerOptions(writer, level, config)

	zapLogger, err := config.Build(opts...)
	if err != nil {
		return logr.Logger{}, err
	}

	return zapr.NewLogger(zapLogger), nil
}

func getLoggerOptions(writer []io.Writer, level *zap.AtomicLevel, config zap.Config) []zap.Option {
	var opts []zap.Option

	// adds file line and name of caller function
	withCaller := zap.WithCaller(true)
	opts = append(opts, withCaller)
	// adds stacktrace to lof entry
	if level.Level() == zap.DebugLevel {
		withStacktrace := zap.AddStacktrace(zc.DebugLevel)
		opts = append(opts, withStacktrace)
	}
	// redirects log output to passed io.Writer
	if len(writer) > 0 {
		syncer := zc.AddSync(writer[0])
		encoder := zc.NewJSONEncoder(config.EncoderConfig)
		withWriter := zap.WrapCore(func(core zc.Core) zc.Core {
			return zc.NewCore(encoder, syncer, *level)
		})
		opts = append(opts, withWriter)
	}
	return opts
}

func parseLogLevel(logLevel string) (*zap.AtomicLevel, error) {
	level, err := zc.ParseLevel(logLevel)
	if err != nil {
		return nil, fmt.Errorf("error parsing to zap logLevel: %w", err)
	}
	al := &zap.AtomicLevel{}
	err = al.UnmarshalText([]byte(level.String()))
	if err != nil {
		return nil, fmt.Errorf("error unmarshal logLevel to atomic: %w", err)
	}
	return al, nil
}
