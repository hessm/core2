# Core

## How to use config?
1. define config struct with all required values (include BaseConfig too!)
````go
type exampleConfig struct {
    core.BaseConfig `mapstructure:",squash"`
    TestValue       string `mapstructure:"testValue"`
}
````

you can also define extra structs inside the config struct:
````go
type exampleConfig struct {
    core.BaseConfig `mapstructure:",squash"`
    TestValue       string `mapstructure:"testValue"`
    OAuth           struct {
        ServerUrl    string `mapstructure:"serverUrl"`
        ClientId     string `mapstructure:"clientId"`
        ClientSecret string `mapstructure:"clientSecret"`
    } `mapstructure:"oAuth"`
}
````

2. Create global config struct instance and load config with LoadConfig() function. Provide Prefix for ENV variables and map with default values
````go
var ExampleConfig exampleConfig

func LoadConfig() error {
	err := core.LoadConfig("EXAMPLE", &ExampleConfig, getDefaults())
	if err != nil {
		return err
	}
	return nil
}

func getDefaults() map[string]any {
    return map[string]any{
    "testValue": "ciao",
    }
}
````