package types

import (
	"github.com/go-logr/logr"
)

const (
	LogrDebugLevel = 1
)

type Logger struct {
	logr.Logger
}

func (l Logger) Debug(msg string, keyAndValues ...any) {
	l.V(LogrDebugLevel).Info(msg, keyAndValues...)
}
