package types

type Environment interface {
	IsHealthy() bool
}
