package core

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/internal/routes"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/types"
)

type Server struct {
	router      *gin.Engine
	baseGroup   *gin.RouterGroup
	environment types.Environment
}

// NewServer creates default gin server with basic route configuration.
func NewServer(environment types.Environment) *Server {
	router := gin.Default()
	baseGroup := routes.AddBasicRoutes(environment, router)

	return &Server{
		router:      router,
		baseGroup:   baseGroup,
		environment: environment,
	}
}

// Run starts up the server on given port.
func (s *Server) Run(port int) error {
	err := s.router.Run(fmt.Sprintf(":%d", port))
	if err != nil {
		return err
	}
	return nil
}

// Add runs given function to add endpoints to gin RouterGroup.
//
// Example of addFunc:
//
//	func addUserRoutes(rg *gin.RouterGroup) {
//		users := rg.Group("/users")
//
//		users.GET("/", func(c *gin.Context) {
//			c.JSON(http.StatusOK, "users")
//		})
//		users.GET("/comments", func(c *gin.Context) {
//			c.JSON(http.StatusOK, "users comments")
//		})
//		users.GET("/pictures", func(c *gin.Context) {
//			c.JSON(http.StatusOK, "users pictures")
//		})
//	}
func (s *Server) Add(addFunc func(*gin.RouterGroup)) {
	addFunc(s.baseGroup)
}
