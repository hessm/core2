package core

import (
	"errors"
	"fmt"
	"github.com/spf13/viper"
	"strings"
)

var viperInstance = viper.New()

// BaseConfig can be used to import the base config parameters in the applications config struct.
// Please use with tag `mapstructure:",squash"`.
type BaseConfig struct {
	LogLevel string `mapstructure:"logLevel"`
	IsDev    bool   `mapstructure:"isDev"`
	Port     int    `mapstructure:"servingPort"`
}

// LoadConfig sets given defaults and read in given config.
func LoadConfig(prefix string, config any, defaults map[string]any) error {
	setDefaults(defaults)

	if err := readConfig(prefix); err != nil {
		return err
	}

	if err := viperInstance.Unmarshal(config); err != nil {
		return err
	}

	return nil
}

func readConfig(prefix string) error {
	viperInstance.SetConfigName("config")
	viperInstance.SetConfigType("yaml")
	viperInstance.AddConfigPath(".")

	viperInstance.SetEnvPrefix(strings.ToTitle(prefix))
	viperInstance.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := viperInstance.ReadInConfig(); err != nil {
		var configFileNotFoundError viper.ConfigFileNotFoundError
		if !errors.As(err, &configFileNotFoundError) {
			return fmt.Errorf("error read in configFile: %w", err)
		}
	}
	viperInstance.AutomaticEnv()
	return nil
}

func setDefaults(defaults map[string]any) {
	viperInstance.SetDefault("logLevel", "info")
	viperInstance.SetDefault("servingPort", 8080)

	for key, value := range defaults {
		viperInstance.SetDefault(key, value)
	}
}
