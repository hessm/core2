package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/types"
	"net/http"
)

func AddHealthRoute(environment types.Environment, g *gin.RouterGroup) {
	g.GET("/health", func(c *gin.Context) {
		if environment.IsHealthy() {
			c.JSON(http.StatusOK, gin.H{
				"status": "healthy",
			})
		} else {
			c.JSON(http.StatusOK, gin.H{
				"status": "unhealthy",
			})
		}
	})
}
