package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/types"
)

func AddBasicRoutes(environment types.Environment, router *gin.Engine) *gin.RouterGroup {
	v1 := router.Group("/v1")

	metricsGroup := v1.Group("/metrics")
	AddHealthRoute(environment, metricsGroup)

	tenancyGroup := v1.Group("/tenants")
	baseGroup := tenancyGroup.Group("/:tenantId")

	return baseGroup
}
